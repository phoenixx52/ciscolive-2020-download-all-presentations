## Cisco Live Europe 2020

The goal of this script is to download all cisco live presentations without clicking everywhere.

## You might have to install couple of extra modules if it's not the case yet

        # python -m pip install --upgrade pip & python -m pip install requests

## To run the script
        
        # python3 getPresentionsCLEUR.py

## Docker commands

Build the container and copy the script to /app

        # docker build --tag cleur-presentations .

Run the container with subdir pdfs mounted from current directory

        # mkdir pdfs
        # docker run -v `pwd`/pdfs:/app/pdfs cleur-presentations

After it is done all pdfs are in pdfs directory
