FROM python:3.7-alpine3.11
COPY . /app
WORKDIR /app
RUN python -m pip install --upgrade pip &\
    python -m pip install requests
CMD python ./getPresentionsCLEUR.py
